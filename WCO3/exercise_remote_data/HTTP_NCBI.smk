import os
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider

HTTP = HTTPRemoteProvider()
NCBI = NCBIRemoteProvider(email="f.feenstra@pl.hanze.nl")

rule all:
    input:
         #HTTP.remote("bioinf.nl/~fennaf/snakemake/test.txt", keep_local = True)
         NCBI.remote("AAH32336.1.fasta", db="protein")

    run:
        #outputName = os.path.basename(input[0])
        outputName = os.path.basename("test.fasta")
        shell("mv {input} {outputName}")