# -*- python -*-
import os
import glob
from os.path import join
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
"""
Grab data from WCO3/data and put that in a directory,
iin myFunc use glob.glob to grab each file and use that to expand samples .
"""

configfile: "config.yaml"

HTTP = HTTPRemoteProvider()

data = HTTP.remote("bioinf.nl/~fennaf/snakemake/WC03/data", keep_local = True)

input_files = expand("{dataF}.bam", dataF = data)

rule all:
    input:
        #expand("data/{sample}.bam", sample = config["samples"])
        #expand("{sample}.bam", sample = config["samples"])
        expand("data/{sample}.bam", sample = config["samples"])

rule downloadFiles:
    output:
        #"data/{sample}.bam"
          "{sample}.bam"
    run:
        shell("wget bioinf.nl/~fennaf/snakemake/WC03/data/{output}")

rule moveFiles:
    input:
         "{sample}.bam"
    output:
        "data/{sample}.bam"
    run:
        shell("mv {input} data/")


def myfunc(wildcards):
    samples = [os.path.basename(x).rstrip(".bam") for x in glob.glob("bioinf.nl/~fennaf/snakemake/WC03/data/"
    )]
    print(samples)
    return samples

rule build_bam_index:
    input:
        myfunc
    output:
        "test.bai"
    message: "Executing Picard  Buildbamindex with some threads on the following files {input}."
    shell:
        "picard BuildBamIndex I= {input} VALIDATION_STRINGENCY=SILENT"

# rule build_bam_index:
#     input:
#         "data/{sample}.bam"
#     output:
#         "data/{sample}.bai"
#     message: "Executing Picard  Buildbamindex with {threads} threads on the following files {input}."
#     shell:
#         "picard BuildBamIndex I= {input} VALIDATION_STRINGENCY=SILENT"

