# -*- python -*-
from os.path import join

configfile: "config.yaml"
include: "report.smk"


workdir: config["wdir"]
outdir = config["outdir"]

rule all:
    input:
         join(outdir,"output.html")

