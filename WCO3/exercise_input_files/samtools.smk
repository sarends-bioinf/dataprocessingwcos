# -*- python -*-
from os.path import join

configfile: "config.yaml"
include: "bwa_map.smk"


workdir: config["wdir"]
outdir = config["outdir"]

rule samtools_sort:
    input:
         join(outdir,"mapped_reads/{sample}.bam")
    output:
         join(outdir,"sorted_reads/{sample}.bam")
    message:
         "executing samtools sort on the following {input} to generate the following {output}."
    shell:
         "samtools sort -T sorted_reads/{wildcards.sample}"
         " -O bam {input} > {output}"

rule sametools_index:
    input:
         join(outdir, "sorted_reads/{sample}.bam")
    output:
          join(outdir,"sorted_reads/{sample}.bam.bai")
    message:
        "executing samtools index on the following {input} to generate the following {output}."
    shell:
         "samtools index {input}"