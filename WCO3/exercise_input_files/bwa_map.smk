# -*- python -*-
from os.path import join

configfile: "config.yaml"

workdir: config["wdir"]
outdir = config["outdir"]

rule bwa_map:
    input:
         genome =config["genome"] + config["ext"],
         sample ="samples/{sample}.fastq"
    output:
          join(outdir,"mapped_reads/{sample}.bam")
    benchmark:
        join(outdir,"benchmarks/{sample}.bwa.benchmark.txt")
    message:
        "executing bwa mem on the following {input} to generate the following {output}"
    shell:
         "bwa mem {input.genome} {input.sample} | samtools view -Sb - > {output}"
         #"bowtie2 -X {input.genome} -U {input.sample} -S {output}"