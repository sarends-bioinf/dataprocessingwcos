# -*- python -*-
from os.path import join

configfile: "config.yaml"
include: "bcftools.smk"


workdir: config["wdir"]
outdir = config["outdir"]

rule report:
    input:
         join(outdir,"calls/all.vcf")
    output:
          join(outdir,"output.html")
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Stijn Arends", T1=input[0])