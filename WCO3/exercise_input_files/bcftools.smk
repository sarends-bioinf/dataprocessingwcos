# -*- python -*-
from os.path import join

configfile: "config.yaml"
include: "samtools.smk"


workdir: config["wdir"]
outdir = config["outdir"]

rule bcftools_call:
    input:
        fa = "genome.fa",
        bam = expand(outdir + "sorted_reads/{sample}.bam", sample = config["samples"]),
        bai = expand(outdir + "sorted_reads/{sample}.bam.bai", sample = config["samples"])
    output:
        join(outdir,"calls/all.vcf")
    message:
        "executing samtools and bcftools on the following {input} to generate the following {output}."
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"