# -*- python -*-
from os.path import join

configfile: "config.yaml"
workdir: config["wdir"]

rule all:
    input:
         join(config["outdir"], "result/out.vcf")

rule bwa_index:
    input:
         join(config["datadir"],"reference.fa")
    output:
          touch(config["outdir"] + "data/bwa_index.done")
    shell:
         "bwa index {input}"

rule bwa_allign1:
    input:
         check = join(config["outdir"], "data/bwa_index.done"),
         genome = join(config["datadir"], config["genome"]),
         reads = join(config["datadir"], "reads.txt")
    output:
          join(config["outdir"], "data/out.sai")
    threads: 8
    shell:
         "bwa aln -t {threads} -I -t 8 {input.genome} {input.reads} > {output}"

rule bwa_allign2:
    input:
         sai = join(config["outdir"], "data/out.sai"),
         genome = join(config["datadir"], config["genome"]),
         reads = join(config["datadir"], "reads.txt")
    output:
          join(config["outdir"], "alligned/out.sam")
    shell:
         "bwa samse {input.genome} {input.sai} {input.reads} > {output}"

rule convert_sam_to_bam:
    input:
         sam = join(config["outdir"], "alligned/out.sam")
    output:
          join(config["outdir"], "data/out.bam")
    shell:
         "samtools view -S -b {input.sam} > {output}"

rule samtools_sort:
    input:
         bam = join(config["outdir"], "data/out.bam")
    output:
          join(config["outdir"], "sorted/out.sorted.bam")
    benchmark:
        "/homes/sarends/jaar_3/Thema_11/dataprocessing/dataprocessingwcos/WCO5/exercise_increase_performance/benchmarks/bwa.benchmark.txt"
    shell:
         "samtools sort {input.bam} -o {output}"

rule picard_remove_duplicates:
    input:
         join(config["outdir"], "sorted/out.sorted.bam")
    output:
          join(config["outdir"], "filtered/out.dedupe.bam")
    shell:
         "java -jar ~/picard/build/libs/picard.jar MarkDuplicates MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}"

rule samtools_index:
    input:
         join(config["outdir"], "filtered/out.dedupe.bam")
    output:
          touch(config["outdir"] + "data/samtools_index.done")
    threads: 8
    shell:
         "samtools index {input}"

rule pileup:
    input:
         check = join(config["outdir"] + "data/samtools_index.done"),
         genome = join(config["datadir"], config["genome"]),
         filtered = join(config["outdir"], "filtered/out.dedupe.bam")
    output:
          join(config["outdir"], "result/out.vcf")
    threads: 8
    shell:
         "samtools mpileup -uf {input.genome} {input.filtered} | bcftools view -> {output}"


