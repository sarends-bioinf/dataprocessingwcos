# -*- python -*-
from os.path import join

configfile: "config.yaml"
workdir: config["wdir"]

rule all:
    input:
         join(config["outdir"], "result/out.vcf")

rule bwa_index:
    input:
         join(config["datadir"],"reference.fa")
    output:
          touch(config["outdir"] + "data/bwa_index.done")
    benchmark:
        join(config["outdir"],"benchmarks/bwa_index.benchmark.txt")
    message: "executing bwa index on the following input: {input}"
    shell:
         "bwa index {input}"

rule bwa_allign1:
    input:
         check = join(config["outdir"], "data/bwa_index.done"),
         genome = join(config["datadir"], config["genome"]),
         reads = join(config["datadir"], "reads.txt")
    output:
          join(config["outdir"], "data/out.sai")
    benchmark:
        join(config["outdir"],"benchmarks/bwa_allign1.benchmark.txt")
    threads: 8
    message: "Performing bwa aln on the file {input.reads} with {threads} threads"
    shell:
         "bwa aln -t {threads} -I -t 8 {input.genome} {input.reads} > {output}"

rule bwa_allign2:
    input:
         sai = join(config["outdir"], "data/out.sai"),
         genome = join(config["datadir"], config["genome"]),
         reads = join(config["datadir"], "reads.txt")
    output:
          join(config["outdir"], "alligned/out.sam")
    benchmark:
        join(config["outdir"],"benchmarks/bwa_allign2.benchmark.txt")
    message: "Performing bwa samse with the following files {input.genome}, {input.sai} and {input.reads}" 
    shell:
         "bwa samse {input.genome} {input.sai} {input.reads} > {output}"

rule convert_sam_to_bam:
    input:
         sam = join(config["outdir"], "alligned/out.sam")
    output:
          join(config["outdir"], "data/out.bam")
    benchmark:
        join(config["outdir"],"benchmarks/samtools_view.benchmark.txt")
    message: "Performing samtools view on {input.sam}"
    shell:
         "samtools view -S -b {input.sam} > {output}"

rule samtools_sort:
    input:
         bam = join(config["outdir"], "data/out.bam")
    output:
          join(config["outdir"], "sorted/out.sorted.bam")
    benchmark:
        join(config["outdir"],"benchmarks/samtools_sort.benchmark.txt")
    message: "Performing samtools sort on {input.bam}"
    shell:
         "samtools sort {input.bam} -o {output}"

rule picard_remove_duplicates:
    input:
         join(config["outdir"], "sorted/out.sorted.bam")
    output:
          join(config["outdir"], "filtered/out.dedupe.bam")
    benchmark:
        join(config["outdir"],"benchmarks/picard_remove_duplicates.benchmark.txt")
    message: "Performing picard remove duplicates on {input}"
    shell:
         "java -jar ~/picard/build/libs/picard.jar MarkDuplicates MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}"

rule samtools_index:
    input:
         join(config["outdir"], "filtered/out.dedupe.bam")
    output:
          touch(config["outdir"] + "data/samtools_index.done")
    benchmark:
        join(config["outdir"],"benchmarks/samtools_index.benchmark.txt")
    threads: 8
    message: "Perorming samtools index on {input} with {threads} threads"
    shell:
         "samtools index {input}"

rule pileup:
    input:
         check = join(config["outdir"] + "data/samtools_index.done"),
         genome = join(config["datadir"], config["genome"]),
         filtered = join(config["outdir"], "filtered/out.dedupe.bam")
    output:
          join(config["outdir"], "result/out.vcf")
    benchmark:
        join(config["outdir"],"benchmarks/samtools_mpileup.benchmark.txt")
    threads: 8
    message: "Performing samtools mpileup with the following files {input.genome} and {input.filtered} with {threads} threads"
    shell:
         "samtools mpileup -uf {input.genome} {input.filtered} | bcftools view -> {output}"


