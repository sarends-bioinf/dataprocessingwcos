#!/bin/bash
source ../../venv/bin/activate
snakemake --snakefile condor_test.smk --cores 16
deactivate